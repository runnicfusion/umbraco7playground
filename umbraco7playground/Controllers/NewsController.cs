﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using umbraco7playground.Models;

namespace umbraco7playground.Controllers
{
    public class CommentsController : SurfaceController
    {
        [HttpPost]
        public ActionResult Post(CommentsModel model)
        {
            if (ModelState.IsValid)
            {
                // Create comment in database and redirect to news item with comment form turned off
                CommentsLogic.InsertComment(model);

                // Redirect to news item
                return Redirect(Umbraco.Content(model.NewsItem).Url + "?hideCommetsForm=false");
            }
            else
            {
                return Redirect(Umbraco.Content(model.NewsItem).Url + "?hideCommetsForm=true");
            }
        }
    }
}
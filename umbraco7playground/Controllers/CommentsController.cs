﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using umbraco7playground.Models;

namespace umbraco7playground.Controllers
{
    public class NewsController : RenderMvcController
    {
        public override ActionResult Index(RenderModel m)
        {
            var hideCommetsForm = Request.Params["hideCommetsForm"];
            var model = new News(m.Content);
            model.HideCommentField = !string.IsNullOrEmpty(hideCommetsForm) ? false : Convert.ToBoolean(hideCommetsForm); //  turn off when posting comment from actionresult PostComment
            return View(model);
        }

        [HttpPost]
        public ActionResult Post(News model)
        {
            if (ModelState.IsValid)
            {
                // Create comment in database and redirect to news item with comment form turned off
                CommentsLogic.InsertComment(model.PostedComment);
                return RedirectToAction("Index", new { hideCommetsForm = true });
            }
            else
            {
                return RedirectToAction("Index", new { hideCommetsForm = true });
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using umbraco7playground.Models;

namespace umbraco7playground.Controllers
{
    public class NewsOverviewController : RenderMvcController
    {
        public override ActionResult Index(RenderModel m)
        {
            var model = new NewsOverview(m.Content);
            model.Results = new NewsOverviewResultSet(); // Create the results viewmodel
            
            // Get parameters for the request
            model.Results.Skip = Request.Params["skip"] != null && Convert.ToInt32(Request.Params["skip"]) != 0 ? Convert.ToInt32(Request.Params["skip"]) : 0;
            int oldSkip = model.Results.Skip; // Needed for replacing the urls in the paging part
            model.Results.Take = Request.Params["take"] != null && Convert.ToInt32(Request.Params["take"]) != 0 ? Convert.ToInt32(Request.Params["take"]) : 6;
            model.Results.Page = Request.Params["page"] != null && Convert.ToInt32(Request.Params["page"]) != 0 ? Convert.ToInt32(Request.Params["page"]) : 1; // Current page; start at page 1 instead of 0
            string prevSortOrder = Request.Params["prevSortOrder"] != null ? Request.Params["prevSortOrder"] : "pubdate,desc"; // for changing the url with the correct value
            string sortOrder = Request.Params["sortOrder"] != null ? Request.Params["sortOrder"] : "pubdate,desc";
            int itemsPerPage = model.ItemsToShow != 0 ? model.ItemsToShow : 6; // Take the value from newsoverview; if 0 than we default take 6 items per page
            // Sorting: [sortfield, asc|desc]
            string sortOrderFieldKey = sortOrder.Split(',')[0].Trim(); // key (sorting field)
            string sortOrderFieldValue = sortOrder.Split(',')[1].Trim(); // value (asc, desc)

            model.Results.SortOrder = sortOrderFieldKey + "," + sortOrderFieldValue; // For populating the view
            model.Results.PrevSortOrder = prevSortOrder;

            // When request is not made from ajax return the complete view; else just the updated data (listview + paging)
            if (!Request.IsAjaxRequest())
            {
                model.Results.ShowLayout = true; // Hide layout for ajax requests
            }

            IEnumerable<News> newsItems = model.Children<News>();
            model.Results.Items = newsItems;

            // Sorting on title
            if (sortOrderFieldKey == "title")
            {
                if (sortOrderFieldValue == "asc")
                {
                    model.Results.Items = model.Results.Items.OrderBy(i => i.Title).ToList();
                }
                else
                {
                    model.Results.Items = model.Results.Items.OrderByDescending(i => i.Title).ToList();
                }
            }
            // Sorting on author
            if(sortOrderFieldKey == "author")
            { 
                if (sortOrderFieldValue == "asc")
                {
                    model.Results.Items = model.Results.Items.OrderBy(i => i.Author.AuthorName).ToList();
                }
                else
                {
                    model.Results.Items = model.Results.Items.OrderByDescending(i => i.Author.AuthorName).ToList();
                }
            }
            // Sorting on publication date
            else if (sortOrderFieldKey == "pubdate")
            {
                if (sortOrderFieldValue == "asc")
                {
                    model.Results.Items = model.Results.Items.OrderBy(i => i.PublicationDate).ToList();
                }
                else
                {
                    model.Results.Items = model.Results.Items.OrderByDescending(i => i.PublicationDate).ToList();
                }
            }

            // Skipping news items for correct paging
            model.Results.Items = model.Results.Items.Skip(model.Results.Skip).Take(model.Results.Take);          
            int nrOfPages = (int)Math.Ceiling((decimal)newsItems.Count() / (decimal)itemsPerPage); // Calc the number of pages
            if (nrOfPages == 0)
            {
                nrOfPages = 1; // Always minimal 1 page; first page is not 0
            }

            // Paging logic
            var pages = new List<Models.Pages>();
            for (int i = 1; i <= nrOfPages; i++)
            {
                // create the new url
                var url = Request.Url.AbsoluteUri;
                string symbol = url.Contains("?") ? "&" : "?"; // Only first param may have an ?, rest &
                // Setting the right skip value
                int skip = (i - 1) * model.Results.Take;
                if (!url.Contains("Skip="))
                {
                    // no skip in old url
                    symbol = url.Contains("?") ? "&" : "?"; // Only first param may have an ?, rest &
                    url = url + symbol + "Skip=" + skip;
                }
                else
                {
                    // Change the old skip value
                    url = url.Replace("Skip=" + model.Results.Skip, "Skip=" + skip);
                }
                // Setting the right take value
                if (!url.Contains("Take="))
                {
                    symbol = url.Contains("?") ? "&" : "?"; // Only first param may have an ?, rest &
                    url = url + symbol + "Take=" + model.Results.Take;
                }
                else
                {
                    // Change the old take value
                    url = url.Replace("Take=" + model.Results.Take, "Take=" + model.Results.Take);
                }
                // Append the sort setting    
                if (!url.Contains("SortOrder="))
                {
                    symbol = url.Contains("?") ? "&" : "?"; // Only first param may have an ?, rest &
                    url = url + symbol + "SortOrder=" + model.Results.SortOrder;
                }
                else
                {
                    // Change the old take value
                    url = url.Replace("SortOrder=" + prevSortOrder, "SortOrder=" + model.Results.SortOrder);
                }

                // Append the page id, for bookmarking only
                if (!url.Contains("Page="))
                {
                    symbol = url.Contains("?") ? "&" : "?"; // Only first param may have an ?, rest &
                    url = url + symbol + "Page=" + i;
                }
                else
                {
                    // Change the old take value
                    url = url.Replace("Page=" + model.Results.Page, "Page=" + i);
                }
                // For every page ad the right data for the view
                pages.Add(new Models.Pages
                {
                    Url = url,
                    isCurrent = i == model.Results.Page ? true : false,
                    IsStart = i == 0 ? true : false,
                    IsEnd = i == nrOfPages ? true : false,
                    PageId = i
                });
            }
            model.Results.Skip = model.Results.Skip + model.Results.Items.Count(); // new skip
            model.Results.Pages = pages; // return the paging var to model
            
            return View(model);
        }
    }
}
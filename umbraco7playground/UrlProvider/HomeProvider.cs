﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.presentation;
using Umbraco.Web.Routing;

namespace umbraco7playground.UrlProvider
{
    public class HomeProvider : IUrlProvider
    {
        public string GetUrl(Umbraco.Web.UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            var content = umbracoContext.ContentCache.GetById(id);
            if (content != null && content.DocumentTypeAlias.ToLower() == "home" && content.Parent != null)
            {
                // Rewrite /Home to /
                return content.Parent.Url;
            }
            return null;
        }

        public IEnumerable<string> GetOtherUrls(Umbraco.Web.UmbracoContext umbracoContext, int id, Uri current)
        {
            return Enumerable.Empty<string>();
        }
    }
}
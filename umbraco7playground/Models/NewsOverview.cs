﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace umbraco7playground.Models
{
    public partial class NewsOverview
    {
        public NewsOverviewResultSet Results { get; set; }
    }

    public class NewsOverviewResultSet
    {
        public bool ShowLayout { get; set; }
        public string SortOrder { get; set; }
        public string PrevSortOrder { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public int Page { get; set; }
        public int ItemsLeft { get; set; }
        public IEnumerable<umbraco7playground.Models.News> Items { get; set; }
        public List<Pages> Pages { get; set; }
        public int NewsOverviewPageId { get; set; }
    }

    public class Pages{
        public int PageId { get; set; }
        public string Url { get; set; }
        public bool isCurrent { get; set; }
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.ModelsBuilder;
using Umbraco.Web;

namespace umbraco7playground.Models
{
    public partial class News
    {
        ///<summary>
		/// Author
		///</summary>
		[ImplementPropertyType("author")]
        public Author Author
        {
            get {
                // I use the MNTP only for returning one author
                var author = this.GetPropertyValue<IEnumerable<IPublishedContent>>("author").FirstOrDefault();
                if(author != null)
                {
                    return author as umbraco7playground.Models.Author;
                }
                return null;
            }
        }

        public IEnumerable<CommentsModel> Comments
        {
            get
            {
                // Get the comment for this news item
                return CommentsLogic.GetComments(this.Id);
            }
        }
        public bool HideCommentField { get; set; }

        public CommentsModel PostedComment { get; set; }
    }
}
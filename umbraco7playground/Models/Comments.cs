﻿using System;
using System.Collections.Generic;
using UIOMatic.Attributes;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace umbraco7playground.Models
{
    [UIOMaticAttribute("Comments on news items", "icon-comments", "icon-comment", ReadOnly = true, SortColumn = "CreatedOn", SortOrder = "desc")]
    [TableName("RWComments")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class CommentsModel
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        [UIOMaticField(Name = "Senders name",  Description = "Senders name", View = UIOMatic.Constants.FieldEditors.Label)]
        [UIOMaticListViewField]       
        public string SenderName { get; set; }

        [UIOMaticField(Name = "Senders email", Description = "Senders email", View = UIOMatic.Constants.FieldEditors.Label)]
        [UIOMaticListViewField]
        [UIOMaticListViewFilter]
        public string SenderEmail { get; set; }

        [UIOMaticField(Name = "Message", Description = "Message", View = UIOMatic.Constants.FieldEditors.Label)]
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        [UIOMaticListViewField]
        public string Message { get; set; }

        [UIOMaticField(Name = "Publish date", Config = "{'format' : '{{value|relativeDate}}'}", Description = "Published on", View = UIOMatic.Constants.FieldEditors.Label)]
        [UIOMaticListViewField]
        [UIOMaticListViewFilter]
        public DateTime CreatedOn { get; set; }

        [UIOMaticField(Name = "News item", Description = "News item page comment belongs to", View = UIOMatic.Constants.FieldEditors.Label)]
        [UIOMaticListViewField]
        [UIOMaticListViewFilter]
        public int NewsItem { get; set; }
    }

    public class CommentsLogic
    {
        public static IEnumerable<CommentsModel> GetComments(int newsItem)
        {
            // Get comments based on news item Id
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var results = db.Query<CommentsModel>("select * from RWComments where NewsItem =" + newsItem);
            return results;
        }
        public static void InsertComment(CommentsModel comment)
        {
            // Get comments based on news item Id
            comment.CreatedOn = DateTime.Now;
            var db = ApplicationContext.Current.DatabaseContext.Database;
            db.Insert(comment);       
        }
    }
}
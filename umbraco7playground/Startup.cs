﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Web.Routing;

namespace umbraco7playground
{
    public class Startup : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            // Register URL Provider(s)
            UrlProviderResolver.Current.InsertTypeBefore<DefaultUrlProvider, UrlProvider.HomeProvider>();

            var ctx = applicationContext.DatabaseContext;
            var db = new DatabaseSchemaHelper(ctx.Database, applicationContext.ProfilingLogger.Logger, ctx.SqlSyntax);

            //Check if the DB table is in database
            if (!db.TableExist("RWComments"))
            {
                //Create DB table
                db.CreateTable<Models.CommentsModel>(false);
            }
        }
    }
}
## Roy's Umbraco 7 playground project
### Admin login:
Inlog: roywagemakers@gmail.com
Password: r0yw@sh3r3!
### Features:
- Umbraco 7.12.3 (nuget)
- Zurb Foundation trough Sass (npm 11.1.0; https://nodejs.org/en/) (Foundation template: https://github.com/zurb/foundation-sites-template)
- News overview with news from an API
- Contact form with Form Editor (Nuget)
- Content page
- UiOMatic comments
### Info
- Build the software in Visual Studio 2017
- Site is in debug mode
- No smtp is provided
- Database is a SQL CE database included in project
######2018 Roy Wagemakers, RoyWagemakers.nl


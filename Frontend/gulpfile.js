var gulp          = require('gulp');
var browserSync   = require('browser-sync').create();
var $             = require('gulp-load-plugins')();
var autoprefixer  = require('autoprefixer');
var addsrc = require('gulp-add-src');


var sassPaths = [
  'node_modules/foundation-sites/scss',
  'node_modules/motion-ui/src'
];

function sass() {
  return gulp.src('scss/app.scss')
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.postcss([
      autoprefixer({ browsers: ['last 2 versions', 'ie >= 9'] })
    ]))
    .pipe(gulp.dest('../umbraco7playground/css'))
    .pipe(browserSync.stream());
};

function js() {
  return gulp.src('js/**/*.js')
    .pipe(addsrc.prepend('node_modules/jquery/dist/jquery.js'))
	.pipe(addsrc.prepend('node_modules/what-input/dist/what-input.js'))
	.pipe(addsrc.prepend('node_modules/foundation-sites/dist/js/foundation.js'))

	.pipe(gulp.dest('../umbraco7playground/scripts'));
}

function serve() {
  browserSync.init({
    server: "./"
  });

  gulp.watch("scss/*.scss", sass);
  gulp.watch("js/*.js", js);
  gulp.watch("*.html").on('change', browserSync.reload);
  gulp.watch("*.cshtml").on('change', browserSync.reload);
}

gulp.task('sass', sass);
gulp.task('js', js);
gulp.task('serve', gulp.series('sass', serve));
gulp.task('default', gulp.series('sass', serve));
gulp.task('serve', gulp.series('js', serve));
gulp.task('default', gulp.series('js', serve));